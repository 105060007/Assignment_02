# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score | Check |
|:----------------------------------------------------------------------------------------------:|:-----:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |  v   |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |  v   |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |  v   |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |  v   |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |  v   |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |    v   |
| Appearance (subjective)                                                                        |  10%  |  v   |
| Other creative features in your game (describe on README.md)                                   |  10%  |  v  | 

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed




## Basic functions
* complete game process
    * load -> menu -> game view -> game over(可上傳成績) -> play again
* follow the basic rules of "小朋友下樓梯"
    * 掉出畫面外即死
    * 隨機產生平台(種類、位置)
    * 地下第xxx階(右上角)
    * 被刺到(畫面上方or尖刺平台)扣血，走到普通平台可回血
    * 走路動畫、平台(輸送帶、彈簧)動畫
    * 按空白鍵可以暫停遊戲，再按一次則繼續
* have correct physical properties & behaviors
    * 人物依據重力往下掉
    * 人物與平台、牆壁、尖刺的合理碰撞和效果
    * enemy與牆壁、人物的碰撞
* sound effects
    * 背景音樂
    * 彈簧、土石鬆動、被刺到、死亡
* database & leaderboard
    * 在menu按enter即可進入leaderboard的畫面(列出前三名)，再按一次enter返回menu
* appearance
    * 使用素材

## Other creative features
* 遊戲畫面左上角有角色頭像，點選可隨機換背景音樂(5首)
* 可以選擇不同角色遊玩(總共5種player跟角色頭像)
* HP用圖案(愛心)顯示
* 共6種平台
    * 普通、向左、向右、彈簧、尖刺
    * 尖刺 -> 踩到會有flash(game.camera.flash)
    * Fake floor(踩上去不能撐10秒)，平台會在人物踩到的地方有emitter(土石鬆落的感覺)
* enemy每隔100秒出現
    * 與牆壁碰撞
    * 有動畫(齒輪轉動)
    * 如果人物碰到，會隨機傳送(window.location)到我做的小畫家、mid proj(論壇)或清大開放式課程