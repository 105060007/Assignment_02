var rankState = {
    create: function () {
        game.add.image(0, 0, 'background');

        var rankLabel = game.add.text(game.width / 2, game.height / 2 + 50
            , 'press the ENTER key to back to menu', { font: '25px Arial', fill: '#000000' });
        rankLabel.anchor.setTo(0.5, 0.5);

        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        enterKey.onDown.add(this.start, this);

        var First = game.add.text(128, 64, '#1 ', { font: '25px Arial', fill: '#000000' });
        var Second = game.add.text(128, 128, '#2 ', { font: '25px Arial', fill: '#000000' });
        var Third = game.add.text(128, 192, '#3 ', { font: '25px Arial', fill: '#000000' });

        // var Name = [];
        // var Score = [];
        var Rank = {Name:"", Score:""};
        
        var i = 0;
        // firebase
        var database = firebase.database().ref('score_list').orderByChild('score').limitToLast(3);

        database.once('value', function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                // console.log(childSnapshot.val().name);
                // console.log(childSnapshot.val().score);
                var x = childSnapshot.val().name;
                var y = childSnapshot.val().score;
                // Name.push(x);
                // Score.push(y);
                if(i==0) Third.text = '#3 ' + x + " " + y;
                if(i==1) Second.text = '#2 ' + x + " " + y;
                if(i==2) First.text = '#1 ' + x + " " + y;

                i+=1;
            })
        })

    },

    start: function () {
        game.state.start('menu');
    },
}