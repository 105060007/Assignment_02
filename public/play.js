var playState = {

    preload: function () {
        game.load.image('bg', 'assets/colored_grass.png');
    },

    create: function () {
        // console.log(game.global.character);
        this.bg = game.add.sprite(0, 0, 'bg');
        game.renderer.renderSession.roundPixels = true;
        game.physics.startSystem(Phaser.Physics.ARCADE);
        this.cursor = game.input.keyboard.createCursorKeys();
        game.physics.arcade.checkCollision.down = false; // 向下掉出畫面會死掉

        
        // pause
	    this.spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.spaceKey.onDown.add(function(){ if(game.paused)game.paused = false; else game.paused = true;  console.log(game.paused)}, this);

        // player
        this.HudPlayer = game.add.sprite(64, 32, 'Hud' + game.global.character);
        this.HudPlayer.scale.setTo(0.5, 0.5);
        this.HudPlayer.inputEnabled = true;
        this.HudPlayer.events.onInputDown.add(this.changeMusic, this);

        this.player = game.add.sprite(game.width / 2, 200, game.global.character);
        game.physics.arcade.enable(this.player);
        this.player.enableBody = true;      // Enable Physics

        this.player.anchor.setTo(0.5, 0.5);
        this.player.body.gravity.y = 500;   // Add vertical g to the player
        this.player.scale.setTo(0.8, 0.8);

        this.player.animations.add('rightwalk', [1, 2], 8, true);
        this.player.animations.add('leftwalk', [3, 4], 8, true);

        // enemy
        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        this.enemies.createMultiple(2, 'enemy');
        game.time.events.loop(100000, this.addEnemy, this);

        // HP
        hearts = [];
        for(var i = 0; i < 5; i++){
            var heart = game.add.sprite(64 + i * 32, 96, 'fullHeart', 0, this.hearts);
            heart.scale.setTo(0.25, 0.25);
            hearts.push(heart);
        }

        /// Particle
        this.emitter = game.add.emitter(0, 0, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        // this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 500;


        // floor group?
        this.floors = game.add.group();      // create group
        this.floors.enableBody = true;       // add physic to the whole group


        // spikes on the top
        this.spike = game.add.sprite(64, 0, 'spike');
        game.physics.arcade.enable(this.spike);
        this.spike.enableBody = true;
        this.spike.body.immovable = true;

        // wall(both side)
        this.wallL = game.add.sprite(0, 0, 'wall');
        game.physics.arcade.enable(this.wallL);
        this.wallL.enableBody = true;
        this.wallL.body.immovable = true;

        this.wallR = game.add.sprite(game.width - 64, 0, 'wall', 0, this.walls);
        game.physics.arcade.enable(this.wallR);
        this.wallR.enableBody = true;
        this.wallR.body.immovable = true;

        this.player.body.collideWorldBounds = true;

        lastTime = 0;   // interval of building platform

        // Display the HP
        // this.HPLabel = game.add.text(64, 64, 'HP: 10', { font: '25px Arial', fill: '#000000' });
        this.HP = 5;
        
        // Display the score
        this.scoreLabel = game.add.text(game.width - 128, 64, 'B0', { font: '25px Arial', fill: '#000000' });
        game.global.score = 0;

        // Sounds
        this.deadSound = game.add.audio('dead');
        this.hurt = game.add.audio('hurt');
        this.jump = game.add.audio('jump');
        this.drop = game.add.audio('drop');

        this.BG0 = game.add.audio('BG0');
        this.BG1 = game.add.audio('BG1');
        this.BG2 = game.add.audio('BG2');
        this.BG3 = game.add.audio('BG3');
        this.BG4 = game.add.audio('BG4');

        this.BG0.play();

    },

    update: function () {
        game.physics.arcade.collide(this.player, this.floors, this.floorEffect, null, this);
        game.physics.arcade.collide(this.player, this.wallL);
        game.physics.arcade.collide(this.player, this.wallR);
        game.physics.arcade.collide(this.player, this.spike, this.hitSpike, null, this);
        game.physics.arcade.collide(this.player, this.fakeFloor, this.blockParticle, null, this);

        game.physics.arcade.overlap(this.player, this.enemies, this.hitEnemy, null, this);
        game.physics.arcade.collide(this.enemies, this.wallL);
        game.physics.arcade.collide(this.enemies, this.wallR);
        //, function(){ console.log("RR"); this.enemies.scale.x *= -1;}, null, this
        this.movePlayer();

        
        this.floors.setAll('body.velocity.y', '-20');
        
        this.createPlatforms();
        this.updateHP();
    },
    
    changeMusic: function(){
        
        var i = game.rnd.pick([0, 1, 2, 3, 4]);
        console.log(i);
        
        this.BG0.stop();
        this.BG1.stop();
        this.BG2.stop();
        this.BG3.stop();
        this.BG4.stop();

        if(i == 0) this.BG0.play();
        if(i == 1) this.BG1.play();
        if(i == 2) this.BG2.play();
        if(i == 3) this.BG3.play();
        if(i == 4) this.BG4.play();
        
    },

    floorEffect: function(player, platform){
        // console.log(platform.key);
        if(platform.key == "fakeFloor"){
            this.emitter.x = player.x;
            this.emitter.y = platform.y + 32;
            this.emitter.start(true, 800, null, 15);
            platform.body.checkCollision.up = false;
            if (player.touchOn !== platform) {
                this.drop.play();
                player.touchOn = platform;;
            }   
            // this.drop.play();         
        }
        else if(platform.key == "conveyRightFloor"){
            player.body.x += 2;
        }
        else if(platform.key == "conveyLeftFloor"){
            player.body.x -= 2;
        }
        else if(platform.key == "bounceFloor"){
            player.body.velocity.y = -500;
            platform.play('bounce');
            this.jump.play();
        }
        else if(platform.key == "spikesFloor"){
            if (player.touchOn !== platform) {
                this.HP -= 1;
                this.hurt.play();
                player.touchOn = platform;
                game.camera.flash(0xff0000, 100);
            }
        }
        else if(platform.key == "normalFloor"){
            if (player.touchOn !== platform) {
                if(this.HP < 5){
                    this.HP += 1;
                } 
                player.touchOn = platform;
            }
        }
    },

    addEnemy: function(){
        
        var enemy = this.enemies.getFirstDead();
        if(!enemy) return;

        // initialize enemy
        
        game.physics.arcade.enable(enemy);
        enemy.body.setSize(24, 24, 4, 4);
        enemy.animations.add('fly', [0, 1], 8, true);
        enemy.animations.play('fly');
        enemy.anchor.setTo(0.5, 0.5);
        enemy.reset(game.width/2, game.height);
        enemy.body.velocity.x = -100;
        enemy.body.velocity.y = -100;
        enemy.body.bounce.x = 1;
        enemy.body.bounce.y = 1;
        enemy.checkWorldBounds = true;
        enemy.outOfBoundsKill = true;
    },

    hitEnemy: function(){
        // alert('停止耍廢^w^!');
        var i = game.rnd.pick([0, 1, 2]);
        if(i==0) window.location = 'http://ocw.nthu.edu.tw/ocw/';
        else if(i==1) window.location = 'https://sowtware-studio-2018.firebaseapp.com/';
        else if(i==2) window.location = 'https://105060007.gitlab.io/AS_01_WebCanvas/';
    },

    createOnePlatform: function () {
        var i = Math.floor(Math.random() * 15);  // position
        var mode = game.rnd.pick([1, 2, 3, 4, 5, 6, 7]);
        // console.log(i);

        if(mode%6 ==1){        // normal
            var platform = game.add.sprite(64 + 32 * i, game.height, 'normalFloor', 0, this.floors);
                
        }
        else if(mode==2){   // spike
            var platform = game.add.sprite(64 + 32 * i, game.height, 'spikesFloor', 0, this.floors);

            platform.body.setSize(192, 20, 0, 20);
            
            // spikes.body.velocity.y = -10;
        }
        else if(mode==3){   // spring
            var platform = game.add.sprite(64 + 32 * i, game.height, 'bounceFloor', 0, this.floors);

            platform.animations.add('bounce', [1, 0], 4);
            platform.frame = 0;
                
        }
        else if(mode==4){   // conveyR
            var platform = game.add.sprite(64 + 32 * i, game.height, 'conveyRightFloor', 0, this.floors);
            platform.animations.add('scroll', [0, 1, 2, 3], 12, true);
            platform.play('scroll');
                
        }
        else if(mode==5){   // conveyL
            var platform = game.add.sprite(64 + 32 * i, game.height, 'conveyLeftFloor', 0, this.floors);
            platform.animations.add('scroll', [0, 1, 2, 3], 12, true);
            platform.play('scroll');   
        }
        else{               // fake?
            var platform = game.add.sprite(64 + 32 * i, game.height, 'fakeFloor', 0, this.floors);
        }
        
        
        game.physics.arcade.enable(platform);
        platform.enableBody = true;
        platform.body.immovable = true;


        platform.body.checkCollision.down = false;
        platform.body.checkCollision.right = false;
        platform.body.checkCollision.left = false;
        
    },

    createPlatforms: function () {
        if (game.time.now > lastTime + 1200) {
            // console.log(lastTime);
            lastTime = game.time.now;
            this.createOnePlatform();
            game.global.score += 1;
        }
    },

    movePlayer: function () {
        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -200;
            this.player.animations.play('leftwalk');
        }
        else if (this.cursor.right.isDown) {
            this.player.body.velocity.x = 200;
            this.player.animations.play('rightwalk');
        }
        else {
            this.player.body.velocity.x = 0;
            this.player.animations.stop();
            this.player.frame = 0;
        }
        if (!this.player.inWorld) this.playerDie();
    },

    hitSpike: function() {
        this.hurt.play();
        game.camera.flash(0xff0000, 100);
        this.HP -= 1;
    },

    updateHP: function () {
        // this.HPLabel.text = 'HP: ' + this.HP;
        if (this.HP == 0) this.playerDie();

        for(var i =0; i<5; i++) hearts[i].visible = true;

        if(this.HP <= 4) hearts[4].visible = false;
        if(this.HP <= 3) hearts[3].visible = false;
        if(this.HP <= 2) hearts[2].visible = false;
        if(this.HP == 1) hearts[1].visible = false;
        this.scoreLabel.text = 'B' + game.global.score;
    },

    playerDie: function () {
        this.BG0.stop();
        this.BG1.stop();
        this.BG2.stop();
        this.BG3.stop();
        this.BG4.stop();
        this.deadSound.play();

        var str="^w^";
        str = prompt('Your name?');
        // console.log(str);

        if (str != null) {

            var newpostref = firebase.database().ref('score_list').push();
            newpostref.set({
                name: str,
                score: game.global.score
            });

            alert('Upload! ' + str + ": " + game.global.score);         
        }
        game.state.start('menu');
    }
};

