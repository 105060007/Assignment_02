var loadState = {
    preload: function(){
        var loadingLabel = game.add.text(game.width/2, 200, 'loading...030', {font: '30px Arial', fill: '#FFFFFF'});
        loadingLabel.anchor.setTo(0.5, 0.5);

        var progressBar = game.add.sprite(game.width/2, 250, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);// sprite will be cropped horizontally

        game.load.image('barV', 'assets/barV.png');
        game.load.image('normalFloor', 'assets/normal.png');
        game.load.spritesheet('bounceFloor', 'assets/bounce_platform.png', 192, 64);
        game.load.image('spikesFloor', 'assets/spike_platform.png');
        game.load.image('fakeFloor', 'assets/fake.png');
        game.load.image('pixel', 'assets/brickBrown.png');
        game.load.spritesheet('conveyLeftFloor', 'assets/conveyorL.png', 192, 64);
        game.load.spritesheet('conveyRightFloor', 'assets/conveyorR.png', 192, 64);
        game.load.spritesheet('enemy', 'assets/enemy.png', 64, 64);
        
        // game.load.image('enemy', 'assets/saw.png');
        game.load.image('spike', 'assets/spikes.png');
        game.load.image('wall', 'assets/wall.png');

        // HUD
        game.load.image('Hudplayer1', 'assets/hudplayer1.png');
        game.load.image('Hudplayer2', 'assets/hudplayer2.png');
        game.load.image('Hudplayer3', 'assets/hudplayer3.png');
        game.load.image('Hudplayer4', 'assets/hudplayer4.png');
        game.load.image('Hudplayer5', 'assets/hudplayer5.png');

        
        game.load.image('fullHeart', 'assets/hudHeart_full.png');

        // load a new asset that we will use in the menu state
        game.load.image('background', 'assets/blue_desert.png');

        // sound
        game.load.audio('dead', 'assets/dead.wav');
        game.load.audio('hurt', 'assets/hurt.mp3');
        game.load.audio('jump', 'assets/jump.mp3');
        game.load.audio('drop', 'assets/drop.wav');
        game.load.audio('BG0', 'assets/BG0.mp3');
        game.load.audio('BG1', 'assets/BG1.mp3');
        game.load.audio('BG2', 'assets/BG2.mp3');
        game.load.audio('BG3', 'assets/BG3.mp3');
        game.load.audio('BG4', 'assets/BG4.mp3');
    },

    create: function(){
        game.state.start('menu');
    }
}