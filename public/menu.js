var menuState = {
    preload: function(){
        game.load.spritesheet('player1', 'assets/player1.png', 66, 100);
        game.load.spritesheet('player2', 'assets/player2.png', 66, 100);
        game.load.spritesheet('player3', 'assets/player3.png', 66, 100);
        game.load.spritesheet('player4', 'assets/player4.png', 66, 100);
        game.load.spritesheet('player5', 'assets/player5.png', 66, 100);
    },

    create: function(){
        game.add.image(0, 0, 'background');


        var nameLabel = game.add.text(game.width/2, 100, 'Leap of Faith', {font: '50px Arial', fill: '#000000'});
        nameLabel.anchor.setTo(0.5, 0.5);



        var chooseLabel = game.add.text(game.width/2, 180, 'click to choose one and start', {font: '30px Arial', fill: '#000000'});
        chooseLabel.anchor.setTo(0.5, 0.5);

        player1 = game.add.sprite(game.width/2-200, 300, 'player1');
        player1.anchor.setTo(0.5, 0.5);
        player1.inputEnabled = true;
        player2 = game.add.sprite(game.width/2-100, 300, 'player2');
        player2.anchor.setTo(0.5, 0.5);
        player2.inputEnabled = true;
        player3 = game.add.sprite(game.width/2, 300, 'player3');
        player3.anchor.setTo(0.5, 0.5);
        player3.inputEnabled = true;
        player4 = game.add.sprite(game.width/2+100, 300, 'player4');
        player4.anchor.setTo(0.5, 0.5);
        player4.inputEnabled = true;
        player5 = game.add.sprite(game.width/2+200, 300, 'player5');
        player5.anchor.setTo(0.5, 0.5);
        player5.inputEnabled = true;

        game.global.character = 'player1';

        player1.events.onInputDown.add(function(){ game.global.character = 'player1'; game.state.start('play');}, this);
        player2.events.onInputDown.add(function(){ game.global.character = 'player2'; game.state.start('play');}, this);
        player3.events.onInputDown.add(function(){ game.global.character = 'player3'; game.state.start('play');}, this);
        player4.events.onInputDown.add(function(){ game.global.character = 'player4'; game.state.start('play');}, this);
        player5.events.onInputDown.add(function(){ game.global.character = 'player5'; game.state.start('play');}, this);

        var scoreLabel = game.add.text(game.width/2, game.height/2
            , 'score: '+game.global.score, {font: '25px Arial', fill: '#000000'});
        scoreLabel.anchor.setTo(0.5, 0.5);
        
        var rankLabel = game.add.text(game.width/2, game.height/2 + 50
            , 'press the SPACE to pause while playing', {font: '25px Arial', fill: '#000000'});
        rankLabel.anchor.setTo(0.5, 0.5);

        var rankLabel = game.add.text(game.width/2, game.height/2 + 100
            , 'press the ENTER key to see the leaderboard', {font: '25px Arial', fill: '#000000'});
        rankLabel.anchor.setTo(0.5, 0.5);
        
        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        enterKey.onDown.add(this.leaderboard, this);
    },

    leaderboard: function(){
        game.state.start('rank');
    }
}