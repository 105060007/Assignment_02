var bootState = {
    preload: function(){
        game.load.image('progressBar', 'assets/barH.png');
    },

    create: function(){
        game.stage.backgroundColor = '#666666';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;

        game.state.start('load');
    }
}