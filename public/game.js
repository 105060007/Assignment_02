var game = new Phaser.Game(768, 960, Phaser.AUTO, "canvas");

// define global var
game.global = { score: 0};

// Add all states
game.state.add('rank', rankState);
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);

game.state.start('boot');